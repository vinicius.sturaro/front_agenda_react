import React, { Component } from 'react';
import { Route } from 'react-router-dom'
import './App.css';
import ListarContatos from './ListarContatos'
import CriarContato from './CriarContato'
import * as ContatosService from './service/ContatosService.js';

class App extends Component {
  state = {
    screen: 'list',
    contacts: []
    
  }

  componentDidMount() {
    ContatosService.getAll().then((contacts)=> {
      this.setState({ contacts })
    })
  }
  removerContato = contact => {
    this.setState( state => ({
      contacts: this.state.contacts.filter((c) => c._id !== contact._id)

    }));
    ContatosService.remove(contact)

  };

  criarContato(contact) {
    ContatosService.novo(contact).then(contact => {
      this.setState(state => ({
        contacts: state.contacts.concat([ contact ])
      }))
    })
  }

  render() {
    return (
      <div>
        <Route exact path='/' render={() => (
          <ListarContatos
            onDeleteContact={this.removerContato}
            contacts={this.state.contacts}
            onNavigate = {() => {
              this.setState({ screen: 'create'})
            }}
          />
        )}/>
        <Route path='/novo' render={({ history }) => (
          <CriarContato
          onCriarContato={(contact) => {
            this.criarContato(contact)
            history.push('/')
          }}
          />
        )}/>
      </div>
    )
  }
}

export default App;
