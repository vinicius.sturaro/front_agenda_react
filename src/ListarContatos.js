import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import escapeRegExp from 'escape-string-regexp'
import sortBy from 'sort-by'

class ListarContatos extends Component {
  static propTypes = {
    contacts: PropTypes.array.isRequired,
    onDeleteContact: PropTypes.func.isRequired
  }

  state = {
    query: ''
  }

  updateQuery = (query) => {
    this.setState({ query: query.trim() })
  }

  clearQuery = () => {
    this.setState({ query: '' })
  }

  render() {
    const { contacts, onDeleteContact } = this.props
    const { query } = this.state

    let showingContacts
    if (query) {
      const match = new RegExp(escapeRegExp(query), 'i')
      showingContacts = contacts.filter((contact) => match.test(contact.name))
    } else {
      showingContacts = contacts
    }

    showingContacts.sort(sortBy('name'))

    return (
      <div className='container-fluid'>
        <div className="form-group has-search">
          <span className="glyphicon glyphicon-search search-icon"></span>
          <input type="text" className="form-control search-input" 
            placeholder="Buscar contato" 
            value={query}
            onChange={(event) => this.updateQuery(event.target.value)} />
            <Link
            to='/novo'
            className='btn btn-primary search-btn'>Adicionar</Link>
        </div>

        {showingContacts.length !== contacts.length && (
          <div className='showing-contacts'>
            <span>Exibindo {showingContacts.length} de {contacts.length} </span>
            <button className='btn' onClick={this.clearQuery}>Mostrar todos</button>
          </div>
        )}

        <ol>
          {showingContacts.map((contact) => (
            <li key={contact._id} className='contact-list-item'>
              <div className='contact-avatar' style={{
                backgroundImage: `url(${contact.avatarURL})`
              }}/>
              <div className='contact-details'>
                <p>{contact.name}</p>
                <p>{contact.email}</p>
              </div>
              <button onClick={() => onDeleteContact(contact)} className='btn btn-danger btn-remover'>
                Remover
              </button>
            </li>
          ))}
        </ol>
      </div>
    )
  }
}

export default ListarContatos