const api =  process.env.REACT_APP_CONTACTS_API_URL || 'http://localhost:3003'

const headers = {
    'Accept':'application/json'    
}

export const getAll = () =>
    fetch(`${api}/api/agenda`, {headers})
        .then(res => res.json())
        .then(data=> data)

export const remove = (contact) => 
    fetch(`${api}/api/agenda/${contact._id}`, { method: 'DELETE', headers})
        .then(res => console.log(res))
        
export const novo = (body) =>
    fetch(`${api}/api/agenda`, {
        method:'POST',
        headers: {
            ...headers,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(res => res.json())