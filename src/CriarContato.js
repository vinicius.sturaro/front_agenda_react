import React, { Component } from 'react'
import serializeForm from 'form-serialize'
import ImagemInput from './ImagemInput'

 class CriarContato extends Component {
    handleSubmit = (e) => {
        e.preventDefault()
        const values = serializeForm(e.target, { hash: true })
        if (this.props.onCriarContato)
        this.props.onCriarContato(values)
    }
  render() {
    return (
    <div>
        <form onSubmit={this.handleSubmit} className='create-contact-form'>
            <ImagemInput
                className='create-contact-avatar-input'
                name='avatarURL'
                maxHeight={64}
            />
            <div className='form-group'>
                <input className='form-control' type='text' name='name' placeholder='Nome'/>
                <br></br>
                <input className='form-control' type='text' name='email' placeholder='Email'/>
                <br></br>
                <button className='btn btn-primary'>Adicionar Contato</button>
            </div>
        </form>
   </div>
   )
 }
}
export default CriarContato